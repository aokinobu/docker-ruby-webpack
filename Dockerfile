FROM ruby:2.3
LABEL maintainer "Nobu Aoki <aoki@sparqlite.com>"


RUN curl -sL https://deb.nodesource.com/setup_13.x | bash -
RUN apt-get install -y nodejs

RUN npm install --global webpack webpack-cli typescript
